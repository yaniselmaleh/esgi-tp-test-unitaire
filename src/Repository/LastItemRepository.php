<?php

namespace App\Repository;

use App\Entity\LastItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LastItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method LastItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method LastItem[]    findAll()
 * @method LastItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LastItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LastItem::class);
    }

    // /**
    //  * @return LastItem[] Returns an array of LastItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LastItem
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
