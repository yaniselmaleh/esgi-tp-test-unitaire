<?php

namespace App\Repository;

use App\Entity\TestApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TestApi|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestApi|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestApi[]    findAll()
 * @method TestApi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestApiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestApi::class);
    }

    // /**
    //  * @return TestApi[] Returns an array of TestApi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestApi
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
