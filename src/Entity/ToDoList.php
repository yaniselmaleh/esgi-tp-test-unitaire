<?php

namespace App\Entity;

use App\Repository\ToDoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="toDoList")
     */
    private $Items;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Item")
     */
    private $lastItem;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="todolist", cascade={"persist", "remove"})
     */
    private $UserP;


    public function canAddItem(Item $item)
    {

//        dump($item->getCreationDate());

        if ($this->lastItem != null){

            $dteDiff  = $this->lastItem->getCreationDate()->diff($item->getCreationDate());
            $secondes =  $dteDiff->days*86400 + $dteDiff->h*3600 + $dteDiff->i*60 + $dteDiff->s;
            if( $secondes > 1800 && count($this->getItems()) < 10){
                return $item;
            }else {
                return null;
            }
        }else {
            return $item;
        }

    }

    public function __construct()
    {
        $this->Items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->Items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->Items->contains($item) && $item != null) {
            $this->Items[] = $item;
            $item->setToDoList($this);
            $this->lastItem = $item;
        }
        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->Items->contains($item)) {
            $this->Items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getToDoList() === $this) {
                $item->setToDoList(null);
            }
        }
        return $this;
    }

    public function getLastItem(): ?Item
    {
        return $this->lastItem;
    }

    public function setLastItem(?Item $lastItem): self
    {
        $this->lastItem = $lastItem;
        return $this;
    }

    public function getUserP(): ?User
    {
        return $this->UserP;
    }

    public function setUserP(?User $UserP)
    {
        if ($this->UserP) {
            return null;
        }

        $this->UserP = $UserP;

        // set (or unset) the owning side of the relation if necessary
        $newTodolist = null === $UserP ? null : $this;
        if ($UserP->getTodolist() !== $newTodolist) {
            $UserP->setTodolist($newTodolist);
        }

        return $this;
    }
}
