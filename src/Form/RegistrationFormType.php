<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('email') 
            // ->add('nom')
            // ->add('prenom')
            // ->add('age')


            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'Adresse email',
                    'class' => 'uk-input uk-form-large uk-margin'
                ]
            ])

            ->add('nom', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nom',
                    'class' => 'uk-input uk-form-large'
                ]
            ])

            ->add('prenom', TextType::class, [
                'attr' => [
                    'placeholder' => 'Prenom',
                    'class' => 'uk-input uk-form-large uk-margin'
                ]
            ])

            ->add('age', IntegerType::class, [
                'attr' => [
                    'placeholder' => 'Age',
                    'class' => 'uk-input uk-form-large'
                ]
            ])

            ->add('agreeTerms', CheckboxType::class, [
                'attr' => [
                    'class' => 'uk-checkbox'
                ],
                'label' => 'J\'accepte de donner mes informations ',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [

                'attr' => [
                    'placeholder' => 'Mot de passe',
                    'class' => 'uk-input uk-form-large uk-margin'
                ],
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 8,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 40,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
