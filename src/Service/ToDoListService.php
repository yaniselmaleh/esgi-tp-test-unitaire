<?php


namespace App\Service;

use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ToDoListService
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function newTodolist(int $userId){

        //Récupération du User
        $user = $this->em->getRepository(User::class)->find($userId);

        //Nouvelle liste a assigner a User
        $todolist = new ToDoList();
        $todolist->setUserP($user);

        $this->em->persist($todolist);
        $this->em->flush();

        return $todolist;
    }

    public function newItem(ToDoList $toDoList, Item $item)
    {

        date_default_timezone_set('Europe/Paris');

        $date = date("d/m/Y H:i:s");
        $dateI = date_create_from_format('d/m/Y H:i:s', $date);
        $item->setCreationDate($dateI);

        if($toDoList->canAddItem($item)){
            $toDoList->addItem($item);
            $this->em->persist($item);
            $this->em->flush();


            $toDoList->setLastItem($item);
            $this->em->persist($toDoList);
            $this->em->flush();

            return $item;
        }

        return null;
    }

    public function getlist($userid)
    {
        $list = $this->em->getRepository(ToDoList::class)->find($userid);
        return $list;
    }
}