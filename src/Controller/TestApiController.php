<?php

namespace App\Controller;

use App\Entity\TestApi;
use App\Form\TestApiType;
use App\Repository\TestApiRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/test/api")
 */
class TestApiController extends AbstractController
{
    /**
     * @Route("/", name="test_api_index", methods={"GET"})
     */
    public function index(TestApiRepository $testApiRepository): Response
    {
        return $this->render('test_api/index.html.twig', [
            'test_apis' => $testApiRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="test_api_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $testApi = new TestApi();
        $form = $this->createForm(TestApiType::class, $testApi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($testApi);
            $entityManager->flush();

            return $this->redirectToRoute('test_api_index');
        }

        return $this->render('test_api/new.html.twig', [
            'test_api' => $testApi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="test_api_show", methods={"GET"})
     */
    public function show(TestApi $testApi): Response
    {
        return $this->render('test_api/show.html.twig', [
            'test_api' => $testApi,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="test_api_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TestApi $testApi): Response
    {
        $form = $this->createForm(TestApiType::class, $testApi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('test_api_index');
        }

        return $this->render('test_api/edit.html.twig', [
            'test_api' => $testApi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="test_api_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TestApi $testApi): Response
    {
        if ($this->isCsrfTokenValid('delete'.$testApi->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($testApi);
            $entityManager->flush();
        }

        return $this->redirectToRoute('test_api_index');
    }
}
