<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\ToDoListService;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Item;
use App\Entity\ToDoList;
use App\Form\ItemType;
use Doctrine\ORM\EntityManagerInterface;


class DefaultController extends AbstractController
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/list", name="list_index", methods={"GET"})
     */
    public function list(ToDoListService $todoservice)
    {
       $item = new Item();
       $form = $this->createForm(ItemType::class, $item);


       return $this->render('list/index.html.twig', [
            'list' => $todoservice->getlist($this->getUser()),
            'form' => $form->createView(),
            'user' => $this->getUser(),
       ]);
    }

    /**
     * @Route("/list/new", name="list_add", methods={"POST"})
     */
    public function addItem(Request $request, ToDoListService $todoservice)
    {

       $item = new Item();
       $form = $this->createForm(ItemType::class, $item);
       $form->handleRequest($request);


       $idList = (int)$request->query->get('idList');
       $list = $todoservice->getlist($idList);
       if($todoservice->newItem($list, $item)){
            $this->addFlash('success', 'Une nouvelle tache à été ajouté !');
       }else{
            $this->addFlash('error', 'Impossible d\'ajouter');
       }

            return $this->redirectToRoute('list_index');
    }

    /**
     * @Route("/list/create", name="list_create", methods={"POST"})
     */
    public function createList(Request $request, ToDoListService $todoservice){

        $userId = (int)$request->query->get('idUser');
        $todoservice->newToDolist($userId);

        return $this->redirectToRoute('list_index');
    }
}
