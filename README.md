# Esgi-ToDoList

<!-- PROJECT LOGO -->
<p align="center">
  <a href="https://symfony.com" target="_blank">
    <img src="https://symfony.com/logos/symfony_black_02.svg">
 </a>

  <h1 align="center">Projet Symfony - esgi-tp-test-unitaire</h1>
  
  
Installation avec Docker
------------

[Symfony]<b>[5]</b> afin de lancer le projet `symfony` veuillez exécuter la commande suivante :


```bash
$ make start 
```

Alternativement avec docker-compose :

```bash
$ docker-compose exec php composer install
$ docker-compose run --rm node yarn install
$ docker-compose run --rm node yarn add sass-loader@^7.0.1 node-sass --dev
```

puis :

```bash
$ make install 
```

Alternativement avec docker-compose :

```bash
$ docker-compose exec php composer install
$ docker-compose run --rm node yarn install
$ docker-compose run --rm node yarn add sass-loader@^7.0.1 node-sass --dev
```


Usage
-----


`localhost:8000`

Avant de créer une liste, veuillez d'abord vous crée un compte /!\



Style avec Sass
-----

```bash
$ make assets_watch 
```

Alternativement avec docker-compose :

```bash
$ docker-compose run --rm node yarn encore dev --watch
```


Tests
-----

Veuillez exécuter la commande suivante pour démarrer les tests :

```bash
$ make php_unit 
```

Alternativement avec docker-compose :

```bash
$ docker-compose exec php bin/phpunit
```
