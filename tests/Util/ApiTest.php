<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase
{
    public function testGetDataApi()
    {
        $client = static::createClient();
        $client->request('GET', '/api/items');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertTrue($client->getResponse()->isSuccessful(), 'test');
    }

    public function testIngrationApi()
    {
//        $user = new \App\Entity\User();
//        $user->setEmail("test@test.fr");
//        $user->setPassword("password");
//        $user->setNom("Nom");
//        $user->setPrenom("Prenom");
//        $user->setAge(20);

        $user = [
            'email' => 'test@test.fr',
            'password' => 'password',
            'nom' => 'Nom',
            'prenom' => 'Prenom',
            'age' => 20
        ];

        $client = static::createClient(['headers' => [
            'User-Agent' => 'PHP console app',
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]]);
        $client->request('POST', '/api/users', [
            'body' => json_encode()
        ]);
        dump($client->getResponse()); die();
//        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

}
 ?>