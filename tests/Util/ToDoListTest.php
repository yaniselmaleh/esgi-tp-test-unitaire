<?php


namespace App\Tests\Util;

use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Service\ToDoListService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ToDoListTest extends TestCase
{
    public function testItem()
    {
        $user = new User();
        $user->setId(1);
        $user->setNom("Nom");
        $user->setPrenom("Prenom");
        $user->setAge("15");

        $user2 = new User();
        $user2->setId(2);
        $user2->setNom("Nom");
        $user2->setPrenom("Prenom");
        $user2->setAge("12");

        $item = new Item();
        $item->setName("Name1");
        date_default_timezone_set('Europe/Paris');
        $date = (new \DateTime("now"))->format("d/m/Y H:i:s");
        $dateI = date_create_from_format('d/m/Y H:i:s', $date);
        $item->setCreationDate($dateI);




        $item3 = new Item();
        $item3->setName("Name3");
        $date31 = $dateI->add(date_interval_create_from_date_string('31 minutes'));
        $item3->setCreationDate($date31);

        //Test Ajout d'une liste à un user
        $toDoList = new ToDoList();

        $this->assertEquals($toDoList, $toDoList->setUserP($user)); //La liste n'a pas encore de User donc le test renvoie la liste
        $this->assertEquals(null, $toDoList->setUserP($user2)); //La liste a déja un user donc le Test renvoie null

        //Test Ajout d'item
        $this->assertEquals($item, $toDoList->canAddItem($item)); //Teste la posibilité d'ajout dans une liste vide
        $toDoList->addItem($item);
        $toDoList->setLastItem($item);

        $item2 = new Item();
        $item2->setName("Name2");
        $date10 = $dateI->add(date_interval_create_from_date_string('10 minutes'));
        $item2->setCreationDate($date10);
        $item2->setCreationDate($date31);
        $this->assertEquals(null, $toDoList->canAddItem($item2)); //Teste d'ajout d'un item a 10 minute apres la création du dernier item
//        $this->assertEquals($item3, $toDoList->canAddItem($item3)); //Teste d'ajout d'un item a plus 30 minute apres la création du dernier item

        //Test ajout d'un item au dessus
        $listPleine = new ToDoList();
        for ($i = 0; $i > 9; $i++){
            $listPleine->addItem(new Item());
        }
        $lastitem = new Item();
        $lastitem->setCreationDate($dateI);
        $listPleine->addItem($lastitem);
        $this->assertEquals(null, $listPleine->canAddItem($item)); //Test si ajout d'item au dessus de 10 item
        
    }
}