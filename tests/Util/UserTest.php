<?php

namespace App\Tests\Util;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testAdd()
    {
        $user = new User();
     
        $user->setNom("Nom");
        $user->setPrenom("Prenom");
        $user->setAge("15");

        $this->assertEquals("Nom", $user->getNom());
        $this->assertEquals("Prenom", $user->getPrenom());

        $this->assertEquals("15", $user->getAge());
        $this->assertIsInt(15, $user->getAge());

    }
}