install:
	docker-compose exec php composer update
	docker-compose exec php composer install
	docker-compose run --rm node yarn install
	docker-compose run --rm node yarn add sass-loader@^7.0.1 node-sass --dev

start:
	docker-compose up -d

stop:
	docker-compose down

migrate:
	docker-compose exec php bin/console doctrine:migration:migrate

migration:
	docker-compose exec php bin/console make:migration

clear_cache:
	docker-compose exec php bin/console c:c

php_unit:
	docker-compose exec php bin/phpunit

assets_watch:
	docker-compose run --rm node yarn encore dev --watch

dsu:
	docker-compose exec php bin/console d:s:u --force